## Geplant

- Slug als default automatisch generieren
- Mehrere  Barometer auswählbar machen.

## Aktuell


## Erledigt

- Beschreibungsfeld für Barometer hinzufügen.
- Slug überprüfen
- Deployen
- admin-Oberfläche aus oder auf geheime URL mappen
- Barometer-App auf Root mappen
- Vote Wert checken, muss zwischen min und max liegen
- Fehlermeldungen bei falschen Eingaben im Setup.
- Default Werte im Formular anzeigen.
- Smileys anzeigen neben radiobuttons
- Cookies einbauen
- Barometer anzeigen
- Durchschnitt formatieren, 2 NKS
- Barometer Stimmabgabe
- Barometer Setup
- Barometer Startseite
- Base Template überarbeiten
- Zeiger in SVG Barometer dynamisch machen
- HTML Template bauen
- Views anlegen
- Models anlegen
- Plan machen
- impressum.html anlegen, mit Text befüllen und verlinken
