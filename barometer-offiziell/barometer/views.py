from django.shortcuts import render, redirect, reverse
from django.utils import timezone
from django.http import HttpResponse, HttpResponseBadRequest
from django.db.models import Avg
from django.template.defaultfilters import slugify
from datetime import datetime, timedelta, time
import uuid

from . import models

# Create your views here.



def start(request):
    """
    Start page of our app.
    """
    if 'slug' in request.POST:
        slug = slugify(request.POST['slug'])
        if slug != "":
            return redirect(reverse("barometer", kwargs={'slug': slug}))
    return render(request, 'barometer/start.html')

def impressum(request):
    return render(request, 'barometer/impressum.html')

def about(request):
    return render(request, 'barometer/about.html')


def set_cookie(response, request, cookie_id):
    if 'barometer' in request.COOKIES:
        return response
    else:
        response.set_cookie('barometer', cookie_id, max_age=4*31*24*60*60)
        return response


def prepare_barometer_view(baro, context, timestamp_from):
    avg = models.Vote.objects.filter(barometer=baro, timestamp__gte=timestamp_from).aggregate(Avg('value'))
    current_month = timezone.now().month
    if current_month >= 3 and current_month < 9:
        semester_start = timezone.now().replace(month=3, day=1)
    elif current_month >=9:
        semester_start = timezone.now().replace(month=9, day=1)
    else:
        semester_start = timezone.now().replace(year = timezone.now().year - 1, month=9, day=1)
    avg_semester = models.Vote.objects.filter(barometer=baro, timestamp__gte=semester_start).aggregate(Avg('value'))
    count = models.Vote.objects.filter(barometer=baro, timestamp__gte=timestamp_from).count()
    count_semester = models.Vote.objects.filter(barometer=baro, timestamp__gte=semester_start).count()
    context['baro'] = baro
    context['semester_start'] = semester_start
    if avg['value__avg'] is None:
        # wir haben keine Stimmen in den letzten 24 Stunden
        context['avg'] = round((baro.maximum + baro.minimum) / 2, 2)
    else:
        context['avg'] = round(avg['value__avg'],2)
    if avg_semester['value__avg'] is None:
        # wir haben keine Stimmen in diesem Semester
        context['avg_semester'] = round((baro.maximum + baro.minimum) / 2, 2)
    else:
        context['avg_semester'] = round(avg_semester['value__avg'],2)
    context['count'] = count
    context['count_semester'] = count_semester
    context['angle'] = ((context['avg']-baro.minimum)/(baro.maximum-baro.minimum)) * 180


def barometer(request, slug):
    if 'barometer' not in request.COOKIES:
        cookie_id = str(uuid.uuid4().hex)
    else:
        cookie_id = request.COOKIES['barometer']
    slugified = slugify(slug)
    if slug != slugified:
        return set_cookie(redirect(reverse("barometer", kwargs={'slug': slugified})), request, cookie_id)
    context = {
        'slug': slug
    }
    try:
        baro = models.Barometer.objects.get(slug=slug)
    except models.Barometer.DoesNotExist:
        # Barometer existiert nicht, anlegen.
        if request.method == 'POST':
            form = models.BarometerForm(request.POST)
            context['form'] = form
            if form.is_valid():
                baro = form.save(commit=False)
                baro.slug = slug
                baro.save()
                return set_cookie(redirect(reverse("barometer", kwargs={'slug': slug})), request, cookie_id)
            else:
                return set_cookie(render(request, 'barometer/setup.html', context), request, cookie_id)

        context['form'] = models.BarometerForm()
        return set_cookie(render(request, 'barometer/setup.html', context), request, cookie_id)
    # Barometer existiert
    # Hier definieren wir das Zeitfenster für Cookie check und Durchschnitt
    timestamp_today = datetime.combine(timezone.now(),time(0))
    timestamp_24 = timezone.now() - timedelta(days=1)
    lastvotes = models.Vote.objects.filter(barometer=baro, cookie=cookie_id, timestamp__gte=timestamp_today).order_by('-timestamp')
    vote_allowed = True
    next_vote = timezone.now()
    if len(lastvotes)>0:
       next_vote = timestamp_today + timedelta(days=1) 
       vote_allowed = False
    if vote_allowed and 'vote' in request.POST:
        vote = models.Vote()
        vote.barometer = baro
        vote.cookie = cookie_id
        vote.value = request.POST['vote']
        try:
            vote.clean()
            vote.save()
        except Exception as e:
            print(e)
            pass
        prepare_barometer_view(baro, context, timestamp_24)
        context['next_vote'] = timestamp_today + timedelta(days=1)
        context['vote_allowed'] = False
        return set_cookie(render(request, 'barometer/barometer.html', context), request, cookie_id)
    if 'result' in request.GET or not vote_allowed:
        prepare_barometer_view(baro, context, timestamp_24)
        context['next_vote'] = next_vote
        context['vote_allowed'] = vote_allowed
        return set_cookie(render(request, 'barometer/barometer.html', context), request, cookie_id)
    context['baro'] = baro
    context['range'] = range(baro.minimum, baro.maximum + 1)
    return set_cookie(render(request, 'barometer/vote.html', context), request, cookie_id)


def show_barometer(request, angle):
    try:
        angle = float(angle)
    except Exception:
        return HttpResponseBadRequest("ERROR: not a valid angle")
    with open('barometer/Baro_stimmung_drehbar_0_plain.svg', 'r', encoding='utf8') as f:
        svg = f.read()
        svg = svg.replace('transform="rotate(0 90.88 88.01)"', 'transform="rotate({} 90.88 88.01)"'.format(angle))
        return HttpResponse(svg, content_type="image/svg+xml")
        







