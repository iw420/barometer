from django.db import models
from django.utils import timezone
from django.forms import ModelForm, TextInput, CheckboxInput, ValidationError

# Create your models here.


class Barometer(models.Model):
    slug = models.SlugField()
    question = models.CharField(default="Wie geht es Ihnen heute?", max_length=280)
    minimum = models.IntegerField(default=0)
    maximum = models.IntegerField(default=6)
    minimum_text = models.CharField(max_length=50, default="", blank=True)
    maximum_text = models.CharField(max_length=50, default="", blank=True)
    show_smileys = models.BooleanField(default=True)
    style = models.TextField(default="smileys")
    title = models.CharField(max_length=100, default="", blank=True)
    description = models.TextField(default="", blank=True)

    def clean(self):
        if self.minimum >= self.maximum:
            raise ValidationError({"minimum": "Minimum muss kleiner als Maximum sein."})
        if self.minimum < -10:
            raise ValidationError({"minimum": "Minimum darf nicht kleiner als -10 sein."})
        if self.maximum > 10:
            raise ValidationError({"maximum": "Maximum darf nicht größer als 10 sein."})
        if self.maximum - self.minimum > 10:
            raise ValidationError({"maximum": "Maximum minus Minumum darf nicht größer als 10 sein."})

    def __str__(self):
        return "{} ({})".format(self.slug, self.question)


class BarometerForm(ModelForm):
    class Meta:
        model = Barometer
        fields = ('title', 'question', 'minimum', 'maximum', 'minimum_text', 'maximum_text', 'show_smileys', 'description')
        widgets = {
                'question': TextInput(attrs={'size': 50}),
                'title': TextInput(attrs={'size': 50}),
                'minimum_text': TextInput(),
                'maximum_text': TextInput(),
                }
        help_texts = {
                'question': "Die Frage aller Fragen!",
                'title': 'Optional: Der Titel wird über der Frage in kleinerer Schrift ausgegeben, z.B. für eine Bezeichung der Gruppe, die das Barometer ausfüllen soll.',
                'minimum': "Die untere Grenze für die Barometer-Werte. Für jeden Wert wird ein Radio-Button angezeigt. Das Barometer zeigt immer nur den relativen Stand innerhalb des Bereichs an, für die Interpretation des numerischen Durchschnitts ist es aber entscheidend.",
                'maximum': "Entsprechend die obere Grenze.",
                'minimum_text': "Optional: Ein textuelles Label, das auf der linken Seite der Radio-Buttons angezeigt wird. Wenn Smileys genutzt werden, dann wird das Label zusätzlich zwischen Smiley und Radio Buttons angezeigt.",
                'maximum_text': "Optional: Entsprechend für die rechte Seite.",
                'show_smileys': "Zusätzlich zu den Texten werden Smileys aus dem Smiley-Barometer angezeigt.",
                'description': 'Optional: Die Beschreibung kann weitere Informationen enthalten und wird unter der Abstimmung und dem Barometer angezeigt.'
                }
        labels = {
                'question': 'Frage',
                'title': 'Titel',
                'minimum_text': 'Minimum Label',
                'maximum_text': 'Maximum Label',
                'show_smileys': 'Smileys als Label verwenden',
                'description': 'Beschreibung'
                }



class Vote(models.Model):
    barometer = models.ForeignKey(Barometer, on_delete=models.CASCADE)
    cookie = models.TextField()
    value = models.IntegerField()
    timestamp = models.DateTimeField(default=timezone.now)

    def clean(self):
        try:
            if int(self.value) < self.barometer.minimum:
                raise ValidationError({'value': 'Der Wert darf nicht kleiner Minimum sein.'})
            if int(self.value) > self.barometer.maximum:
                raise ValidationError({'value': 'Der Wert darf nicht größer Maximum sein.'})
        except ValueError as e:
            raise ValidationError(e)

    def __str__(self):
        return "{}: {} | {} | {}".format(self.cookie, self.value, self.timestamp, self.barometer.slug)

    
