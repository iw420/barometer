from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Student

# Create your views here.
def index(request):
    return render(request, "barometer_basis/index.html")

def result(request):
    return render(request, 'barometer_basis/result.html')    

def save(request):    
    if 'done' in request.POST:
        print(request.POST)
        #  mit "vote" im request.POST sind die radio-inputs gemeint
        if "vote" in request.POST:
            stimme = request.POST.get("vote","")
        # cookie wird als 'id' gespeichert            
        # cookie_id = ...
        # ein student wird erstelt
            student = Student(vote = stimme)            
            student.save()
            print(student)
    return redirect("result")
    
def setcookie(request):
    html = HttpResponse("Set Cookie")
    if request.COOKIES.get('visits'):
        html.set_cookie('BarometerCookie', 'Your Cookie', max_age= 4*31*24*60*60) #Sekunden bis August
        value = int(request.COOKIES.get('visits'))
        html.set_cookie('visits', value + 1)
    else:
        value = 1   #Wenn visits > 1 dann nicht mehr abstimmen können?
        html.set_cookie('visits', value)
        html.set_cookie('BarometerCookie')
    return html

def showcookie(request):
    html = HttpResponse("Get Cookie")
    return html


## Beispielfunktionen aus der Session vom 30.4.
#  Nur zum Skizzieren der Cookie-Funktion, die wir brauchen.
#  Das macht bis jetzt noch nichts und ist nicht getestet!

def create_random_cookie():
    # Dummy, natürlich muss hier immer ein neuer Code generiert werden.
    return 'askldjTYWEY12341234'

def cookietest(request):
    html = HttpResponse("Set Cookie")
    if 'BarometerCookie' not in request.COOKIES:
        cookie = create_random_cookie()
        html.set_cookie('BarometerCookie', cookie, max_age= 4*31*24*60*60) #Sekunden bis August
    else:
        cookie = request.COOKIES.get('BarometerCookie')
    return html