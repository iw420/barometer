from django.apps import AppConfig


class BarometerConfig(AppConfig):
    name = 'barometer'
