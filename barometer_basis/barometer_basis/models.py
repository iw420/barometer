from django.db import models
from datetime import datetime

# Create your models here.
class Student(models.Model):
    vote = models.IntegerField()
    #cookie_id = ...        
    timestamp = models.DateTimeField(auto_now_add=True)